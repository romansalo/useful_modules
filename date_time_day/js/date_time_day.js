
/*** ClockPicker ***/
(function ($, Drupal) {
    Drupal.behaviors.clockPicker = {
        attach: function (context, settings) {

            $('.clockpicker').clockpicker();

        }
    };
})(jQuery, Drupal);
