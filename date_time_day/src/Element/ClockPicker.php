<?php

/**
 * @file
 * Contains \Drupal\date_time_day\Element\ClockPicker.
 */

namespace Drupal\date_time_day\Element;

use Drupal\Core\Render\Element\Textfield;

/**
 * Provides an ClockPicker element.
 *
 * @FormElement("clock_picker")
 */
class ClockPicker extends Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $element = parent::getInfo();
    $element['#attached']['library'][] = 'date_time_day/clockpicker';

    $element['#attributes']['pattern'] = '([01]?[0-9]|2[0-3]):[0-5][0-9]';
    $element['#size'] = 12;
    $element['#attributes']['title'] = 'hh:mm';
    $element['#attributes']['class'] = ['form-control'];
    $element['#prefix'] = '<div class="clock-wrapper"><div class="clockpicker" data-placement="right" data-align="bottom" data-autoclose="true">';
    $element['#suffix'] = '<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span></div></div>';

    return $element;
  }

}
