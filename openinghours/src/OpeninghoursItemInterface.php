<?php

namespace Drupal\custom_openinghours;

use Drupal\Core\Field\FieldItemInterface;

/**
 * Defines an interface for the custom_openinghours field item.
 *
 * @property string $monday_from
 * @property string $monday_to
 *
 * @property string $tuesday_from
 * @property string $tuesday_to
 *
 * @property string $wednesday_from
 * @property string $wednesday_to
 *
 * @property string $thursday_from
 * @property string $thursday_to
 *
 * @property string $friday_from
 * @property string $friday_to
 *
 * @property string $saterday_from
 * @property string $saterday_to
 *
 * @property string $sunday_from
 * @property string $sunday_to
 */
interface OpeninghoursItemInterface extends FieldItemInterface {

  public static function getDayNames();

  public static function getDayLabels($langcode = NULL);

  public function getReadableTime($property, $format = 'H:i');

}
