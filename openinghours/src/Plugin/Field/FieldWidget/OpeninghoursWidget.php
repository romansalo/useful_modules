<?php

namespace Drupal\custom_openinghours\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\custom_openinghours\Plugin\Field\FieldType\OpeninghoursItem;

/**
 * Plugin implementation.
 *
 * @FieldWidget(
 *   id = "custom_openinghours_default",
 *   label = @Translation("Opening hours"),
 *   field_types = {
 *     "custom_openinghours"
 *   }
 * )
 */
class OpeninghoursWidget extends WidgetBase {

  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    foreach (OpeninghoursItem::getDayNames() as $day) {
      $settings['placeholder_' . $day . '_from'] = '';
      $settings['placeholder_' . $day . '_to'] = '';
    }

    return $settings;
  }

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\custom_openinghours\OpeninghoursItemInterface $item */
    $item = $items[$delta];

    $element['openinghours'] = [
      '#type' => 'vertical_tabs',
      '#title' => t('Opening hours'),
    ];

    foreach (OpeninghoursItem::getDayLabels() as $day => $day_label) {
      $element['openinghours_' . $day] = [
        '#type' => 'details',
        '#title' => $day_label,
        '#description' => t('Add openinghours for @day', ['@day' => $day_label]),
        '#group' => 'openinghours',
      ];
      $element['openinghours_' . $day][$day . '_from'] = [
        '#type' => 'date',
        '#attributes' => [
          'type' => 'time',
        ],
        '#default_value' => $item->getReadableTime($day . '_from'),
        '#min' => '00:00',
        '#max' => '24:00',
        '#step' => 900,
      ];
      $element['openinghours_' . $day][$day . '_to'] = [
        '#type' => 'date',
        '#attributes' => [
          'type' => 'time',
        ],
        '#default_value' => $item->getReadableTime($day . '_to'),
        '#min' => '00:00',
        '#max' => '24:00',
        '#step' => 900,
      ];
    }

    // If cardinality is 1, ensure a label is output for the field by wrapping
    // it in a fieldset element.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      $element += [
        '#type' => 'fieldset',
      ];
      $element['#attributes']['class'][] = 'fieldgroup';
      $element['#attributes']['class'][] = 'form-composite';
    }

    return $element;
  }

  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $days = OpeninghoursItem::getDayNames();

    $messaged_values = [];
    foreach ($values as $delta => $value) {
      foreach ($days as $day) {

        $key = 'openinghours_' . $day;
        if (!isset($value[$key])) {
          continue;
        }

        $messaged_values[$delta][$day . '_from'] = !empty($value[$key][$day . '_from'])
          ? intval(strtotime($value[$key][$day . '_from'] . ' UTC', 0))
          : NULL;
        $messaged_values[$delta][$day . '_to']   = !empty($value[$key][$day . '_to'])
          ? intval(strtotime($value[$key][$day . '_to'] . ' UTC', 0))
          : NULL;
      }
    }

    return $messaged_values;
  }

}
