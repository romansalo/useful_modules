<?php

namespace Drupal\custom_openinghours\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\custom_openinghours\OpeninghoursItemInterface;

/**
 * Plugin implementation.
 *
 * @FieldType(
 *   id = "custom_openinghours",
 *   label = @Translation("Opening hours"),
 *   description = @Translation("Stores opening hours of a store"),
 *   default_widget = "custom_openinghours_default",
 *   default_formatter = "custom_openinghours"
 * )
 */
class OpeninghoursItem extends FieldItemBase implements OpeninghoursItemInterface {

  public static function getDayNames() {
    return [
      1 => 'monday',
      2 => 'tuesday',
      3 => 'wednesday',
      4 => 'thursday',
      5 => 'friday',
      6 => 'saturday',
      7 => 'sunday',
    ];
  }

  public static function getDayLabels($langcode = NULL) {
    $options = [];
    if ($langcode) {
      $options['langcode'] = $langcode;
    }
    return array_combine(self::getDayNames(), [
      t('Monday', [], $options),
      t('Tuesday', [], $options),
      t('Wednesday', [], $options),
      t('Thursday', [], $options),
      t('Friday', [], $options),
      t('Saturday', [], $options),
      t('Sunday', [], $options),
    ]);
  }

  public function getReadableTime($property, $format = 'H:i') {
    $time = $this->get($property)->getValue();
    if ($time) {
      return gmdate($format, $time);
    }
    return '';
  }

  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    foreach (self::getDayLabels() as $day => $day_label) {
      $properties[$day . '_from'] = DataDefinition::create('integer')
        ->setLabel(t('@day from', ['@day' => $day_label]));
      $properties[$day . '_to'] = DataDefinition::create('integer')
        ->setLabel(t('@day to', ['@day' => $day_label]));
    }

    return $properties;
  }

  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [];
    foreach (self::getDayLabels('en') as $day => $day_label) {
      $columns[$day . '_from'] = [
        'description' => $day_label . ' from',
        'type' => 'int',
        'length' => 5,
        'not null' => FALSE,
      ];
      $columns[$day . '_to'] = [
        'description' => $day_label . ' to',
        'type' => 'int',
        'length' => 5,
        'not null' => FALSE,
      ];
    }

    return ['columns' => $columns];
  }

  public function isEmpty() {
    $empty = TRUE;
    foreach (self::getDayNames() as $day) {
      if ($this->get($day . '_from') && $this->get($day . '_to')) {
        $empty = FALSE;
        break;
      }
    }

    return $empty;
  }

}
