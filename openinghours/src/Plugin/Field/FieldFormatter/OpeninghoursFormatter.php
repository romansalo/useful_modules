<?php

namespace Drupal\custom_openinghours\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\custom_openinghours\Plugin\Field\FieldType\OpeninghoursItem;

/**
 * Plugin implementation.
 *
 * @FieldFormatter(
 *   id = "custom_openinghours",
 *   label = @Translation("Opening hours"),
 *   field_types = {
 *     "custom_openinghours"
 *   }
 * )
 */
class OpeninghoursFormatter extends FormatterBase {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $openinghours = [];

      foreach (OpeninghoursItem::getDayNames() as $day_of_week => $day) {
        $openinghours[$day]['day_of_week'] = $day_of_week;
        $openinghours[$day]['from'] = $item->getReadableTime($day . '_from');
        $openinghours[$day]['to'] = $item->getReadableTime($day . '_to');
      }

      $elements[$delta] = [
        '#theme' => 'custom_openinghours',
        '#openinghours' => $openinghours,
      ];
    }

    if (!empty($elements)) {
      $elements['#attached']['library'][] = 'custom_openinghours/openinghours';
    }

    return $elements;
  }

}
