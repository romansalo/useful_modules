<?php

namespace Drupal\gpnl_charibase_integrations\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\gpnl_charibase_integrations\Service\SoapClientServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process a queue.
 *
 * @QueueWorker(
 *   id = "update_last_access_date",
 *   title = @Translation("Update user last access date."),
 *   cron = {"time" = 60}
 * )
 */
class AccessDateQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The SOAP client service manager.
   *
   * @var \Drupal\gpnl_charibase_integrations\Service\SoapClientServiceInterface
   */
  protected $soapClientService;

  /**
   * AccessDateQueueWorker constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    SoapClientServiceInterface $soap_client_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->soapClientService = $soap_client_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('gpnl_charibase_integrations.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->soapClientService->updateLastAccessDateHandler($data['uid']);
  }

}
