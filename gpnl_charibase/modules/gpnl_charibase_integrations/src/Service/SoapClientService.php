<?php

namespace Drupal\gpnl_charibase_integrations\Service;

use CommerceGuys\Addressing\AddressFormat\AddressField;
use Drupal\address\FieldHelper;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\gpnl_charibase\Service\ConnectionManagerInterface;
use Drupal\user\UserInterface;
use GuzzleHttp\Client;
use Meng\AsyncSoap\Guzzle\Factory;

/**
 * Class SoapClientService.
 */
class SoapClientService implements SoapClientServiceInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The SOAP connection manager.
   *
   * @var \Drupal\gpnl_charibase\Service\ConnectionManagerInterface
   */
  protected $connectionManager;

  /**
   * The current database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * SoapClientService constructor.
   */
  public function __construct(
    ModuleHandlerInterface $module_handler,
    EntityTypeManagerInterface $entity_type_manager,
    ConnectionManagerInterface $connection_manager,
    Connection $database,
    LoggerChannelFactoryInterface $logger_factory
  ) {
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->connectionManager = $connection_manager;
    $this->database = $database;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function sendData($user_id) {
    $connection_args = $this->getConnectionArguments();
    $factory = new Factory();

    foreach ($connection_args as $connection_arg) {
      $user_data = $this->getUserData($user_id, $connection_arg);
      $body_options = [];

      // Get the body options.
      foreach (explode(';', $connection_arg['soap_body_options']) as $values) {
        $explode = explode('=', $values);
        $body_options[$explode[0]] = $explode[1];
      }

      $body_options['trace'] = 1;
      $client = $factory->create(new Client(), $connection_arg['soap_wsdl'], $body_options);
      // Set up the SOAP data.
      $soap_data = [$connection_arg['soap_action'] => (array) $user_data];
      // Perform SOAP action and save result in soap_result.
      $soap_result = $client->call($connection_arg['soap_action'], $soap_data);

      return $this->soapResultHandler($soap_result, $user_data);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getUserData($uid, array $connection_arg) {
    $profile_values = $this->getUserFieldValues($uid, 'profile', 'profile');
    $account_values = $this->getUserFieldValues($uid, 'user', 'user');

    $data = $profile_values + $account_values;

    $list = explode("\n", $connection_arg['mapping_list']);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');

    $values = [];
    foreach ($list as $string) {
      $item_mapping = explode('|', $string);
      $values[$item_mapping[1]] = $data[$item_mapping[0]];
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserFieldValues($uid, $entity_id, $bundle) {
    if ($entity_id === 'profile') {
      if (!$this->moduleHandler->moduleExists($entity_id)) {
        return new \Exception('The "profile" module does not exist');
      }
      $profiles = $this->entityTypeManager
        ->getStorage($entity_id)
        ->loadByProperties([
          'uid' => $uid,
          'type' => $bundle,
        ]);
      /** @var \Drupal\profile\Entity\ProfileInterface $entity */
      $entity = reset($profiles);
    }
    else {
      /** @var \Drupal\user\UserInterface $entity */
      $entity = $this->entityTypeManager
        ->getStorage($entity_id)
        ->load($uid);
    }

    if (!$entity) {
      return [];
    }

    $components = $this->getDisplayComponents($entity_id, $bundle);
    $data = [];
    foreach ($components as $field_name => $component) {
      // Address field.
      if (count($explode = explode('.', $field_name)) > 1) {
        if ($entity->hasField($explode[0])) {
          $data[$field_name] = $entity->get($explode[0])->{$explode[1]};
          continue;
        }
      }
      // Date field.
      if (isset($component['type']) && $component['type'] === 'datetime_default') {
        $data[$field_name] = strtotime($entity->get($field_name)->value);
        continue;
      }

      if ($value = $entity->get($field_name)->value) {
        $data[$field_name] = $entity->get($field_name)->value;
      }
      else {
        continue;
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayComponents($entity_id, $bundle) {
    /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $display */
    $display = $this->entityTypeManager
      ->getStorage('entity_view_display')
      ->load($entity_id . '.' . $bundle . '.soap');

    $components = $display->getComponents();
    if (count($components) == 0) {
      return [];
    }

    // Fields mapping. Core with Address module.
    $address_fields = [
      AddressField::ADDRESS_LINE1 => FieldHelper::getPropertyName(AddressField::ADDRESS_LINE1),
      AddressField::ADDRESS_LINE2 => FieldHelper::getPropertyName(AddressField::ADDRESS_LINE2),
      AddressField::ADMINISTRATIVE_AREA => FieldHelper::getPropertyName(AddressField::ADMINISTRATIVE_AREA),
      AddressField::LOCALITY => FieldHelper::getPropertyName(AddressField::LOCALITY),
      AddressField::POSTAL_CODE => FieldHelper::getPropertyName(AddressField::POSTAL_CODE),
      AddressField::ORGANIZATION => FieldHelper::getPropertyName(AddressField::ORGANIZATION),
      AddressField::GIVEN_NAME => FieldHelper::getPropertyName(AddressField::GIVEN_NAME),
      AddressField::ADDITIONAL_NAME => FieldHelper::getPropertyName(AddressField::ADDITIONAL_NAME),
      AddressField::FAMILY_NAME => FieldHelper::getPropertyName(AddressField::FAMILY_NAME),
    ];

    foreach ($components as $field_name => $component) {
      // Address field.
      if (isset($component['type']) && $component['type'] === 'address_default') {

        // Make new address field names.
        foreach ($address_fields as $address_field) {
          $components[$field_name . '.' . $address_field] = [];
        }

      }
    }

    return $components;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectionArguments() {
    $active_connections = $this->connectionManager->getConnections(TRUE);
    if (count($active_connections) == 0) {
      return [];
    }
    $data = [];
    /** @var \Drupal\gpnl_charibase\Entity\Integration $active_connection */
    foreach ($active_connections as $key => $active_connection) {
      $data[$key] = $active_connection->toArray();
      $data[$key]['name'] = $data[$key]['label'];
      $data[$key]['aid'] = '1';
      // @TODO
      $data[$key]['ip'] = '185.95.21.12';
      $data[$key]['domain'] = 'NL';
    }

    return $data;
  }

  /**
   * Log failed SOAP request.
   *
   * @param array $variables
   *   Should be passed %name %surname and %message in $variables.
   *
   * @return bool
   *   FALSE.
   */
  protected function logFailedRequest(array $variables) {
    $this->loggerFactory->get(self::MODULE_NAME)
      ->error('SoapClient: CB returned error for user: %name %surname. Error: %message', $variables);

    return FALSE;
  }

  /**
   * Log success SOAP request.
   *
   * @param array $variables
   *   Should be passed %name %surname and in $variables.
   *
   * @return bool
   *   TRUE.
   */
  protected function logSuccessRequest(array $variables) {
    $this->loggerFactory->get(self::MODULE_NAME)
      ->error('SoapClient: All data was passed successfully for the user: %name %surname.', $variables);

    return TRUE;
  }

  /**
   * Log error to watchdog or return TRUE if data was sent successfully.
   *
   * @param mixed $result
   *   The result after request.
   * @param array $user_data
   *   User data which was send.
   */
  protected function soapResultHandler($result, array $user_data) {
    // If it's an object there's only 1 result, otherwise an array.
    if (is_object($result->SaveVolunteerResult->Melding)) {
      // Save the response in these vars.
      $code = $result->SaveVolunteerResult->Melding->Code;
      $meld = $result->SaveVolunteerResult->Melding->Meldingtekst;
    }
    else {
      // We only check the first response.
      $code = $result->SaveVolunteerResult->Melding[0]->Code;
      $meld = $result->SaveVolunteerResult->Melding[0]->Meldingtekst;
    }

    $variables = [
      '%name' => $user_data['voornaam'],
      '%surname' => $user_data['achternaam'],
      '%message' => $meld,
    ];
    // Now we save failed request to the Drupal watchdog.
    if ($code < 0) {
      $this->logFailedRequest($variables);
    }
    else {
      $this->logSuccessRequest($variables);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendActualUsersData() {
    $date = new \DateTime('Monday');
    $week_interval = new \DateInterval('P1W');
    $week_interval->invert = 1;
    /** @var \DateTime $start_of_week */
    $start_of_week = $date->add($week_interval);
    $start_of_week_timestamp = $start_of_week->getTimestamp();

    $end_of_week_timestamp = new \DateTime();;
    $end_of_week_timestamp = $end_of_week_timestamp->getTimestamp();

    $uids = $this->database->select(self::TABLE_NAME, 'sd')
      ->fields('sd', ['uid'])
      ->condition('sd.last_access', $start_of_week_timestamp, '>=')
      ->condition('sd.last_access', $end_of_week_timestamp, '<=')
      ->execute()->fetchCol();

    if (count($uids)) {
      /** @var \Drupal\Core\Queue\DatabaseQueue $queue */
      $queue = \Drupal::queue('send_data_to_soap_server');

      foreach ($uids as $uid) {
        $queue->createItem(['uid' => $uid]);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function updateUserDataTable(UserInterface $user, $delete = FALSE) {
    $exist = $this->isExistInCustomTable($user->id());

    if (!$exist && !$delete) {
      $this->insertUserDataTable($user);
    }
    elseif (!$exist && $delete) {
      $this->insertUserDataTable($user, time());
    }

    $query = $this->database->update(self::TABLE_NAME);
    if ($delete) {
      $query = $query->fields([
        'end_date' => time(),
      ]);
    }
    else {
      $query->fields(
        [
          'email' => $user->getEmail(),
          'last_updated' => $user->get('changed')->value,
          'last_access' => $user->getLastAccessedTime(),
        ]);
    }

    return $query->condition('uid', $user->id(), '=')->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function insertUserDataTable(UserInterface $user, $end_date = NULL) {
    $query = $this->database->insert(self::TABLE_NAME)->fields(
      [
        'uid' => $user->id(),
        'email' => $user->getEmail(),
        'last_updated' => $user->get('changed')->value,
        'last_access' => $user->getLastAccessedTime(),
      ]);
    if ($end_date) {
      $query->fields([
        'end_date' => $end_date,
      ]);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function updateLastAccessDate() {
    $uids = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->execute();

    /** @var \Drupal\Core\Queue\DatabaseQueue $queue */
    $queue = \Drupal::queue('update_last_access_date');

    foreach ($uids as $uid) {
      $queue->createItem(['uid' => $uid]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateLastAccessDateHandler($user_id) {
    /** @var \Drupal\user\UserInterface $user */
    $user = \Drupal::entityTypeManager()
      ->getStorage('user')
      ->load($user_id);

    if ($user) {
      $this->updateUserDataTable($user);
    }
  }

  /**
   * Return false if data is missing or return uid if exist.
   *
   * @param int $user_id
   *   The user id.
   *
   * @return mixed
   *   FALSE if not exist in custom table.
   */
  protected function isExistInCustomTable($user_id) {
    return $this->database->select(self::TABLE_NAME, 'sd')
      ->fields('sd', ['uid'])
      ->condition('sd.uid', $user_id)
      ->execute()->fetchField();
  }

}
