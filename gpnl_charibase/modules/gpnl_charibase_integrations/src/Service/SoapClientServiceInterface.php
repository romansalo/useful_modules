<?php

namespace Drupal\gpnl_charibase_integrations\Service;

use Drupal\user\UserInterface;

/**
 * Base interface definition for SoapClientServiceInterface service.
 */
interface SoapClientServiceInterface {

  /**
   * The module name.
   */
  const MODULE_NAME = 'gpnl_charibase_integrations';

  /**
   * The custom database table name.
   */
  const TABLE_NAME = 'soap_data';

  /**
   * Send data to the SOAP Server.
   *
   * @param int $user_id
   *   The user id.
   *
   * @return mixed
   *   Requested result.
   */
  public function sendData($user_id);

  /**
   * Get user data from the SOAP view mode.
   *
   * @param int $uid
   *   The user id.
   * @param array $connection_arg
   *   Arguments from the Integration entity.
   *
   * @return array
   *   Values from Account and Profile fields.
   */
  public function getUserData($uid, array $connection_arg);

  /**
   * Pass all actual users to QueueWorker.
   */
  public function sendActualUsersData();

  /**
   * Get field values from profile and an account.
   *
   * @param int $uid
   *   The user id.
   * @param string $entity_id
   *   The entity id.
   * @param string $bundle
   *   Bundle for the entity.
   *
   * @return array
   *   Field values from entity.
   */
  public function getUserFieldValues($uid, $entity_id, $bundle);

  /**
   * Get connection arguments.
   *
   * @return array
   *   All arguments for connections.
   */
  public function getConnectionArguments();

  /**
   * Update SOAP user data for the custom table.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user account.
   * @param bool $delete
   *   Set user end_date to table.
   *
   * @return mixed
   *   Executed query.
   */
  public function updateUserDataTable(UserInterface $user, $delete = FALSE);

  /**
   * Insert SOAP user data for the custom table.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user account.
   * @param string $end_date
   *   Set user end_date to table.
   *
   * @return mixed
   *   Executed query.
   */
  public function insertUserDataTable(UserInterface $user, $end_date = NULL);

  /**
   * Get display components.
   *
   * @param string $entity_id
   *   The entity id.
   * @param string $bundle
   *   The entity bundle.
   *
   * @return array
   *   Field names for the mapping.
   */
  public function getDisplayComponents($entity_id, $bundle);

  /**
   * Update user last access date.
   */
  public function updateLastAccessDate();

  /**
   * Called from AccessDateQueueWorker.
   *
   * @param int $user_id
   *   The user id.
   */
  public function updateLastAccessDateHandler($user_id);

}
