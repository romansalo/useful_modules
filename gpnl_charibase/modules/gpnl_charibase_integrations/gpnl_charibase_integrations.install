<?php

/**
 * @file
 * Functions for the gpnl_charibase_integrations.install.
 */

use Drupal\gpnl_charibase_integrations\Service\SoapClientServiceInterface;

/**
 * Implements hook_uninstall().
 */
function gpnl_charibase_integrations_uninstall() {
  // Delete module configurations.
  $install_source = drupal_get_path('module', SoapClientServiceInterface::MODULE_NAME) . '/config/install';
  $optional_source = drupal_get_path('module', SoapClientServiceInterface::MODULE_NAME) . '/config/optional';

  $configs = file_scan_directory($install_source, '/.*\.yml$/') + file_scan_directory($optional_source, '/.*\.yml$/');
  foreach ($configs as $file) {
    Drupal::configFactory()->getEditable($file->name)->delete();
  }
}

/**
 * Implements hook_schema().
 *
 * Defines the database tables used by this module.
 *
 * @see hook_schema()
 *
 * @ingroup gpnl_charibase_integrations
 */
function gpnl_charibase_integrations_schema() {
  $schema['soap_data'] = [
    'description' => 'Stores users information.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Primary Key: Unique person ID.',
      ],
      'uid' => [
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'User ID.',
      ],
      'email' => [
        'type' => 'varchar',
        'length' => 50,
        'default' => NULL,
        'description' => 'Person email.',
      ],
      'last_updated' => [
        'type' => 'varchar',
        'length' => 20,
        'default' => NULL,
        'description' => 'Last updated date.',
      ],
      'last_access' => [
        'type' => 'varchar',
        'length' => 20,
        'default' => NULL,
        'description' => 'Last access date.',
      ],
      'end_date' => [
        'type' => 'varchar',
        'length' => 20,
        'default' => NULL,
        'description' => 'User end date.',
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'uid' => ['uid'],
      'email' => ['email'],
    ],
  ];

  return $schema;
}
