<?php

namespace Drupal\gpnl_charibase\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Integration entities.
 */
class IntegrationListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Environment');
    $header['active'] = $this->t('Active');
    $header['soap_action'] = $this->t('SOAP Action');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['active'] = $entity->isActive() ? $this->t('Yes') : $this->t('No');
    $row['soap_action'] = $entity->getAction();

    return $row + parent::buildRow($entity);
  }

}
