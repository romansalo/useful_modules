<?php

namespace Drupal\gpnl_charibase\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\gpnl_charibase\Entity\Integration;

/**
 * Basic Integration entity controller.
 */
class IntegrationController extends ControllerBase {

  /**
   * Return the label of an integration as the page title.
   *
   * @param \Drupal\gpnl_charibase\Entity\Integration $integration
   *   An integration entity.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Label of an integration.
   */
  public function getTitle(Integration $integration) {
    return $this->t('Edit %integration', [
      '%integration' => $integration->label,
    ]);
  }

}
