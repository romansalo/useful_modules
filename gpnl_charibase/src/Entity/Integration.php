<?php

namespace Drupal\gpnl_charibase\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\gpnl_charibase\Service\IntegrationInterface;

/**
 * Defines the Integration entity.
 *
 * @ConfigEntityType(
 *   id = "integration",
 *   label = @Translation("Integration"),
 *   handlers = {
 *     "list_builder" = "Drupal\gpnl_charibase\Controller\IntegrationListBuilder",
 *     "form" = {
 *       "edit" = "Drupal\gpnl_charibase\Form\IntegrationEntityForm",
 *     }
 *   },
 *   config_prefix = "integration",
 *   admin_permission = "administer soap connections",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "collection" = "/admin/config/charibase",
 *     "edit-form" = "/admin/config/charibase/{integration}",
 *   }
 * )
 */
class Integration extends ConfigEntityBase implements IntegrationInterface {

  use StringTranslationTrait;

  /**
   * The Integration ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Integration label.
   *
   * @var string
   */
  public $label;

  /**
   * Whether this Integration is active.
   *
   * @var bool
   */
  public $active = TRUE;

  /**
   * SOAP action.
   *
   * @var string
   */
  public $soap_action;

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return $this->active;
  }

  /**
   * {@inheritdoc}
   */
  public function getAction() {
    return $this->soap_action;
  }

}
