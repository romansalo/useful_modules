<?php

namespace Drupal\gpnl_charibase\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gpnl_charibase_integrations\Service\SoapClientServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base connections entity configuration form.
 *
 * @ingroup gpnl_charibase
 */
class IntegrationEntityForm extends EntityForm {

  /**
   * Entity query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQueryFactory;

  /**
   * The SOAP client service manager.
   *
   * @var \Drupal\gpnl_charibase_integrations\Service\SoapClientServiceInterface
   */
  protected $soapClientService;

  /**
   * Constructs an Integration object.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   An entity query factory for the Integration entity type.
   * @param \Drupal\gpnl_charibase_integrations\Service\SoapClientServiceInterface $soap_client_service
   *   SOAP connection manager.
   */
  public function __construct(QueryFactory $query_factory, SoapClientServiceInterface $soap_client_service) {
    $this->entityQueryFactory = $query_factory;
    $this->soapClientService = $soap_client_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query'),
      $container->get('gpnl_charibase_integrations.client')
    );
  }

  /**
   * Get the title of the integration.
   *
   * @return string
   *   The label of the entity.
   */
  public function getTitle() {
    return $this->t('Edit %integration', [
      '%integration' => $this->entity->label,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    // Return a specific entity type ID instead.
    return 'integration_' . $this->entity->id() . '_form';
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\gpnl_charibase\Entity\Integration $integration */
    $integration = $this->entity;

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#description' => $this->t('Human-readable label for this integration.'),
      '#default_value' => $integration->label,
    ];
    $form['active'] = [
      '#title' => $this->t('Active'),
      '#type' => 'checkbox',
      '#description' => $this->t('Is this integration active?'),
      '#default_value' => $integration->isActive(),
    ];

    $form['auth_details'] = [
      '#title' => $this->t('Authentication details'),
      '#type' => 'fieldset',
    ];
    $form['auth_details']['token'] = [
      '#title' => $this->t('Token'),
      '#description' => $this->t('Token.'),
      '#type' => 'textfield',
      '#default_value' => $integration->get('token') ? $integration->get('token') : '63501742-127E-4AFD-A33B-1AC626752BE1',
    ];
    $form['auth_details']['rate_limit'] = [
      '#title' => $this->t('Rate limit'),
      '#type' => 'textfield',
      '#default_value' => $integration->get('rate_limit') ? $integration->get('rate_limit') : '10000',
    ];
    $form['auth_details']['current_rate_limit'] = [
      '#title' => $this->t('Current rate limit'),
      '#type' => 'textfield',
      '#default_value' => $integration->get('current_rate_limit') ? $integration->get('current_rate_limit') : '10000',
    ];
    $form['auth_details']['date_rate_limit'] = [
      '#title' => $this->t('Date rate limit'),
      '#type' => 'datetime',
      '#default_value' => $integration->get('date_rate_limit') ? DrupalDateTime::createFromTimestamp($integration->get('current_rate_limit')) : DrupalDateTime::createFromTimestamp('1368605191'),
    ];

    $form['auth_details']['soap'] = [
      '#title' => $this->t('SOAP details'),
      '#type' => 'fieldset',
    ];
    $form['auth_details']['soap']['soap_wsdl'] = [
      '#title' => $this->t('WSDL point'),
      '#type' => 'textfield',
      '#default_value' => $integration->get('soap_wsdl'),
      '#required' => TRUE,
    ];
    $form['auth_details']['soap']['soap_action'] = [
      '#title' => $this->t('SOAP action method'),
      '#type' => 'textfield',
      '#default_value' => $integration->get('soap_action') ? $integration->get('soap_action') : 'SaveVolunteer',
      '#required' => TRUE,
    ];
    $form['auth_details']['soap']['soap_header_options'] = [
      '#title' => $this->t('SOAP header options'),
      '#type' => 'textfield',
      '#default_value' => $integration->get('soap_header_options') ? $integration->get('soap_header_options') : '',
    ];
    $form['auth_details']['soap']['soap_header_namespace'] = [
      '#title' => $this->t('SOAP header namespace'),
      '#type' => 'textfield',
      '#default_value' => $integration->get('soap_header_namespace') ? $integration->get('soap_header_namespace') : '',
    ];
    $form['auth_details']['soap']['soap_body_options'] = [
      '#title' => $this->t('SOAP body options'),
      '#type' => 'textfield',
      '#default_value' => $integration->get('soap_body_options') ? $integration->get('soap_body_options') : 'soap_version=SOAP_1_2;exceptions=0',
      '#required' => TRUE,
    ];
    $form['auth_details']['soap']['soap_body_function'] = [
      '#title' => $this->t('SOAP function'),
      '#type' => 'textfield',
      '#default_value' => $integration->get('soap_body_function') ? $integration->get('soap_body_function') : 'CB_user',
    ];

    $form['auth_details']['mapping'] = [
      '#title' => $this->t('Mapping'),
      '#type' => 'fieldset',
    ];
    $form['auth_details']['mapping']['mapping_list'] = [
      '#title' => $this->t('Drupal|WSDL'),
      '#type' => 'textarea',
      '#default_value' => $integration->get('mapping_list') ? $integration->get('mapping_list') : '',
      '#description' => '<strong>' . $this->t('You can use follow ids from Drupal side:') . '</strong>' . implode(', ', $this->getFieldsIds()),
      '#required' => TRUE,
    ];
    $wsdl_fields = [
      'AccessCode',
      'registratienummer',
      'voornaam',
      'achternaam',
      'postcode',
      'plaatsnaam',
      'land',
      'telefoon',
      'mobiel',
      'emailadres',
      'begindatum',
      'einddatum',
    ];
    $form['auth_details']['mapping']['description'] = [
      '#type' => 'markup',
      '#prefix' => '<strong>WSDL Fields:</strong><ul><li>',
      '#markup' => implode('</li><li>', $wsdl_fields),
      '#suffix' => '</li></ul>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\gpnl_charibase\Entity\Integration $integration */
    $integration = $this->getEntity();
    $date = $form_state->getValue('date_rate_limit')->getTimestamp();
    $integration->set('date_rate_limit', $date);
    $status = $integration->save();

    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label integration.', [
        '%label' => $integration->label(),
      ]));
    }
    else {
      $this->messenger()
        ->addMessage($this->t('There was an error while saving the %label integration.', [
          '%label' => $integration->label(),
        ]));
    }

    $form_state->setRedirect('gpnl_charibase.collection');
  }

  /**
   * Get profile and account field ids.
   *
   * @return array
   *   Account and Profile filed ids.
   */
  private function getFieldsIds() {
    $profile_field_ids = $this->soapClientService->getDisplayComponents('profile', 'profile');
    $account_field_ids = $this->soapClientService->getDisplayComponents('user', 'user');

    $components = $profile_field_ids + $account_field_ids;

    return array_keys($components);
  }

}
