<?php

namespace Drupal\gpnl_charibase\Service;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Integration entity.
 */
interface IntegrationInterface extends ConfigEntityInterface {

  /**
   * Check whether the Integration is active.
   *
   * If an integration is active, it should be possible to establish a
   * connection to the endpoint provided. If it isn't, and a GET request is
   * made, an exception will be thrown.
   *
   * @return bool
   *   Whether the Integration is active.
   */
  public function isActive();

  /**
   * Get SOAP action.
   *
   * @return string
   *   Action method from theWSDL file.
   */
  public function getAction();

}
