<?php

namespace Drupal\gpnl_charibase\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class ConnectionManager.
 *
 * @package Drupal\gpnl_charibase\Service
 */
class ConnectionManager implements ConnectionManagerInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ConnectionManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnections($active = FALSE) {
    if ($active) {
      $connections = $this->entityTypeManager
        ->getStorage('integration')
        ->loadByProperties([
          'active' => TRUE,
        ]);
    }
    else {
      $connections = $this->entityTypeManager
        ->getStorage('integration')
        ->loadMultiple();
    }

    if (!$connections) {
      return [];
    }
    return $connections;
  }

  /**
   * Get all connections.
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]
   *   The Inregration entity.
   */
  public function getAllConnections() {
    $connections = $this->entityTypeManager
      ->getStorage('integration')
      ->loadMultiple();

    if (!$connections) {
      return [];
    }
    return $connections;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectionById($id) {
    $connection = $this->entityTypeManager
      ->getStorage('integration')
      ->loadByProperties([
        'id' => $id,
      ]);

    return reset($connection);
  }

}
