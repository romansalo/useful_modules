<?php

namespace Drupal\gpnl_charibase\Service;

/**
 * Base interface definition for ConnectionServiceInterface service.
 */
interface ConnectionManagerInterface {

  /**
   * The module name.
   */
  const MODULE_NAME = 'gpnl_charibase';

  /**
   * Get all or active available connections.
   *
   * @param bool $active
   *   Connection status.
   *
   * @return mixed
   *   List of connections.
   */
  public function getConnections($active = FALSE);

  /**
   * Get connection by id.
   *
   * @param int $id
   *   Connection ID.
   *
   * @return mixed
   *   Connection object.
   */
  public function getConnectionById($id);

}
