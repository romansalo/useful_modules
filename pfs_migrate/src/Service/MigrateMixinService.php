<?php

namespace Drupal\pfs_migrate\Service;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MigrateMixinService
 *
 * @package Drupal\pfs_migrate\Service
 */
class MigrateMixinService implements ContainerFactoryPluginInterface {

  /**
   * The tempStore.
   *
   * @var $tempStore \Drupal\Core\TempStore\PrivateTempStoreFactory
   *
   */
  protected $tempStore;

  /**
   * MigrateMixinService constructor.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory) {
    $this->tempStore = $temp_store_factory->get('pfs_migrate');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('tempstore.private')
    );
  }
}
