<?php

namespace Drupal\pfs_migrate\Service;

use Drupal\Core\Database\Database;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Class ResourceMixinMigrateService
 * @package Drupal\pfs_migrate\Service
 */
class ResourceMixinMigrateService extends MigrateMixinService {

  /**
   * {@inheritdoc}
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory) {
    parent::__construct($temp_store_factory);
  }

  /**
   * @param bool $start_slash
   * @return array
   */
  public function dataList($start_slash = FALSE) {
    $urls = $this->tempStore->get('resource_mixin__node_migrate_urls');

    if ($start_slash) {
      foreach ($urls as &$url) {
        $url = preg_replace("#^https://www.roguewave.com/#", "", trim($url));
      }
    }
    else {
      foreach ($urls as &$url) {
        $url = preg_replace("#^https://www.roguewave.com#", "", trim($url));
      }
    }

    return $urls;
  }

  /**
   * Return node ids by node url.
   */
  public function getNodeId() {
    $connection = Database::getConnection('default', 'migrate');

    $redirect = $connection->select('redirect', 'r');
    $redirect->condition('r.redirect_source__path', $this->dataList(TRUE), 'IN');
    $redirect->fields('r', ['redirect_redirect__uri']);
    $redirect_source = $redirect->execute()->fetchCol();

    if (count($redirect_source)) {
      foreach ($redirect_source as &$path) {
        $path = str_replace('internal:', '', $path);
      }
    }
    $all_urls = array_merge($this->dataList(), $redirect_source);

    $query = $connection->select('url_alias', 'a');
    $query->condition('a.alias', $all_urls, 'IN');
    $query->fields('a', ['source', 'alias']);
    $source = $query->execute()->fetchCol();
    foreach ($source as &$id) {
      $id = preg_replace("#^/node/#", "", $id);
    }

    return $source;
  }

}