<?php

namespace Drupal\pfs_migrate\Plugin\migrate\source;

use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\pfs_migrate\Service\ResourceMixinMigrateService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal 8 file source from database.
 *
 * @MigrateSource(
 *   id = "resource_mixin_media",
 *   source_provider = "file",
 *   source_module = "file"
 * )
 */
class ResourceMixinMedia extends SqlBase {

  /**
   * The resourceService.
   *
   * @var $resourceService \Drupal\pfs_migrate\Service\ResourceMixinMigrateService
   *
   */
  protected $resourceService;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration,
    StateInterface $state,
    ResourceMixinMigrateService $resource_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state);
    $this->resourceService = $resource_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('resources.migrate')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $nids = $this->resourceService->getNodeId();

    $query = $this->select('file_managed', 'f');
    $query->condition('f.uri', 'temporary://%', 'NOT LIKE');
    $query->leftJoin('node__field_resource_file', 'fr', 'f.fid = fr.field_resource_file_target_id');
    $query->leftJoin('node__field_eventimage', 'fe', 'f.fid = fe.field_eventimage_target_id');
    if ($nids) {
      $or = $query->orConditionGroup();
      $or->condition('fe.entity_id', $nids, 'IN');
      $or->condition('fr.entity_id', $nids, 'IN');

      $query->condition($or);
    }
    else {
      $query->condition('fr.entity_id', [0], 'IN');
    }
    $query->fields('f', [
      'fid',
      'langcode',
      'filename',
      'uri',
      'filemime',
    ]);
    $query->fields('fe', ['field_eventimage_target_id']);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'fid' => $this->t('File id'),
      'langcode' => $this->t('Langcode'),
      'filename' => $this->t('File name'),
      'uri' => $this->t('File uri'),
      'filemime' => $this->t('File MIME Type'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['fid']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $filemime = $row->getSourceProperty('filemime');
    switch ($filemime) {
      case 'image/png':
        $bundle = 'image';
        break;

      default:
        $bundle = 'file';
    }
    $row->setSourceProperty('_bundle', $bundle);
  }

}
