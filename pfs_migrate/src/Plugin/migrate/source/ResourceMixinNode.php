<?php

namespace Drupal\pfs_migrate\Plugin\migrate\source;

use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\pfs_migrate\Service\ResourceMixinMigrateService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Source plugin for beer content.
 *
 * @MigrateSource(
 *   id = "resource_mixin_node"
 * )
 */
class ResourceMixinNode extends SqlBase {

  /**
   * The resourceService.
   *
   * @var $resourceService \Drupal\pfs_migrate\Service\ResourceMixinMigrateService
   *
   */
  protected $resourceService;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration,
    StateInterface $state,
    ResourceMixinMigrateService $resource_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state);
    $this->resourceService = $resource_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('resources.migrate')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'nid' => $this->t('Nid'),
      'vid' => $this->t('Vid'),
      'langcode' => $this->t('Langcode'),
      'title' => $this->t('Title'),
      'promote' => $this->t('Promote'),
      'default_langcode' => $this->t('Default langcode'),
      'body_value' => $this->t('Body value'),
      'body_summary' => $this->t('Body summary'),
      'body_format' => $this->t('Body format'),
      'field_type_of_resource_value' => $this->t('Resource type'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'nid' => [
        'type' => 'integer',
        'alias' => 'n',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $nids = $this->resourceService->getNodeId();

    $query = $this->select('node_field_data', 'n');

    $or_type = $query->orConditionGroup();
    $or_type->condition('n.type', 'resources');
    $or_type->condition('n.type', 'event_item');
    $or_type->condition('n.type', 'page');

    $query->condition($or_type);

    if ($nids) {
      $query->condition('n.nid', $nids, 'IN');
    }
    else {
      $query->condition('n.nid', [0], 'IN');
    }

    $query->leftJoin('node__field_eventtype', 'et', 'n.nid = et.entity_id');
    $query->leftJoin('node__field_type_of_resource', 'tr', 'n.nid = tr.entity_id');

    $query->innerJoin('node__body', 'nb', 'n.nid = nb.entity_id');
    $query->leftJoin('node__field_resource_file', 'rf', 'n.nid = rf.entity_id');
    $query->leftJoin('node__field_eventimage', 'fei', 'n.nid = fei.entity_id');

    $query->leftJoin('node__field_resourceid', 'rid', 'n.nid = rid.entity_id');
    $query->leftJoin('node__field_resourcedate', 'rdt', 'n.nid = rdt.entity_id');
    $query->fields('n', [
      'nid',
      'vid',
      'status',
      'langcode',
      'title',
      'promote',
      'default_langcode',
    ]);
    $query->fields('nb', [
      'body_value',
      'body_summary',
      'body_format',
    ]);
    $query->fields('tr', ['field_type_of_resource_value']);
    $query->fields('rf', ['field_resource_file_target_id']);
    $query->fields('fei', ['field_eventimage_target_id']);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $nid = $row->getSourceProperty('nid');
    $query = $this->select('url_alias', 'ua')
      ->fields('ua', ['alias']);
    $query->condition('ua.source', '/node/' . $nid);
    $alias = $query->execute()->fetchField();
    if (!empty($alias)) {
      $row->setSourceProperty('_alias', $alias);
    }

    $topic = $this->select('node__field_resource_topics', 'frt');
    $topic->condition('frt.entity_id', $nid);
    $topic->leftJoin('taxonomy_term_field_data', 'ttd', 'frt.field_resource_topics_target_id = ttd.tid');
    $topic->fields('ttd', ['name']);
    $topic_data = $topic->execute()->fetchCol();
    if ($topic_data) {
      $row->setSourceProperty('_field_topics', implode(',', $topic_data));
    }
    else {
      $row->setSourceProperty('_field_topics', []);
    }

    return parent::prepareRow($row);
  }

}