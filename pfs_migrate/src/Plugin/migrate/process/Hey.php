<?php

namespace Drupal\pfs_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * This plugin sets missing values on the destination.
 *
 * @link https://www.drupal.org/node/2135313 Online handbook documentation for default_value process plugin @endlink
 *
 * @MigrateProcessPlugin(
 *   id = "hey"
 * )
 */
class Hey extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $additional = $this->configuration['additional'];
    return !empty($additional) ? 'hey ' . $additional : 'hey';
  }

}
