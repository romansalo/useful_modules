<?php

namespace Drupal\pfs_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\taxonomy\Entity\Term;

/**
 * Map the download family to the correct taxonomy term.
 *
 * @MigrateProcessPlugin(
 *   id = "map_family"
 * )
 */
class MapFamily extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Ignore this field if there is no value for this field in the feed

    if (empty($value)) {
      return NULL;
    }
    else {
      // $location = $this->getTerm($value);
      if($value == 'macintosh' || $value == 'Macintosh') {
        // Force the key to the correct family name
        $value = 'MacOS';
      }
      $result = \Drupal::entityQuery('taxonomy_term')
        ->condition('name', $value . "%", 'LIKE')
        ->condition('vid', 'operating_system_family')
        ->execute();
      $terms = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadMultiple($result);
      // Check if term already exists
      if ($terms) {
        $term = reset($terms);
      } else {
        $term = Term::create([
          'name' => $value,
          'vid'  => 'operating_system_family',
        ]);
        $term->save();
      } 
      return $term->id();
    }
  }
}
