<?php

namespace Drupal\pfs_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\MigrateSkipRowException;

/**
 * This plugin sets missing values on the destination.
 *
 * @link https://www.drupal.org/node/2135313 Online handbook documentation for default_value process plugin @endlink
 *
 * @MigrateProcessPlugin(
 *   id = "skip_on_update"
 * )
 */
class SkipOnUpdate extends ProcessPluginBase {
  public function row($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if(!empty($row->getIdMap()['destid1'])) {
      throw new MigrateSkipRowException();
    }
    return $value;
  }


  public function process($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if(!empty($row->getIdMap()['destid1'])) {
      throw new MigrateSkipProcessException();
    }
    return $value;
  }
}
