<?php

namespace Drupal\pfs_migrate\Plugin\migrate\process;

use Drupal\Component\Utility\UrlHelper;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Check the link URL and prepend 'internal:/' to internal links.
 *
 * @MigrateProcessPlugin(
 *   id = "map_link"
 * )
 */
class MapLink extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Take care of the one rogue external URL: goo.gl/8XODc3
    if (substr($value, 0, 3) == 'goo') {
      $value = 'http://' . $value;
    }
    // Check if the URL is external to Drupal
    if (UrlHelper::isExternal($value)) {
      $uri = $value;
    }
    // Otherwise prepend 'internal:/' to the URL
    else {
      $uri = 'internal:/' . $value;
    }
    return $uri;
  }
}
