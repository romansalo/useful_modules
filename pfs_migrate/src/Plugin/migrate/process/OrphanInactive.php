<?php

namespace Drupal\pfs_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;

/**
 * This plugin sets missing values on the destination.
 *
 * @link https://www.drupal.org/node/2135313 Online handbook documentation for default_value process plugin @endlink
 *
 * @MigrateProcessPlugin(
 *   id = "pfs_orphan_inactive"
 * )
 */
class OrphanInactive extends ProcessPluginBase {
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // If value is empty and row migration doesn't exist, skip row.
    if(empty($value) && empty($row->getIdMap()['destid1'])) {
      throw new MigrateSkipRowException();
    }
    // If value empty and field empty, skip row.
    elseif (empty($value)
    && !empty($row->getIdMap()['destid1'])
      && ($entity = \Drupal::entityManager()->getStorage('perforce_release')->load($row->getIdMap()['destid1']))
      && ($entity->get($destination_property)->isEmpty() || $entity->get($destination_property)->first()->getValue()['value'] == '0')
    ) {
      throw new MigrateSkipRowException();
    }
    return $value;
  }
}
