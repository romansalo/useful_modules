<?php

namespace Drupal\pfs_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * This plugin sets missing values on the destination.
 *
 * @link https://www.drupal.org/node/2135313 Online handbook documentation for default_value process plugin @endlink
 *
 * @MigrateProcessPlugin(
 *   id = "list_to_term"
 * )
 */
class ListToTerm extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
//    $source = $this->configuration['source'];
//    $properties = is_string($source) ? array($source) : $source;
//    $return = array();
//    foreach ($properties as $property) {
//      if ($property || (string) $property === '0') {
//        $is_source = TRUE;
//        if ($property[0] == '@') {
//          $property = preg_replace_callback('/^(@?)((?:@@)*)([^@]|$)/', function ($matches) use (&$is_source) {
//            // If there are an odd number of @ in the beginning, it's a
//            // destination.
//            $is_source = empty($matches[1]);
//            // Remove the possible escaping and do not lose the terminating
//            // non-@ either.
//            return str_replace('@@', '@', $matches[2]) . $matches[3];
//          }, $property);
//        }
//        if ($is_source) {
//          $return[] = $row->getSourceProperty($property);
//        }
//        else {
//          $return[] = $row->getDestinationProperty($property);
//        }
//      }
//      else {
//        $return[] = $value;
//      }
//    }
//    taxonomy_term_load_multiple_by_name()
   if(isset($value['value']) && is_string($value['value'])) {
     if($terms = taxonomy_term_load_multiple_by_name($value['value'], 'operating_system_family')) {
//       $term = reset($terms);
//       object_log('term', $term);
//       return $term->id();
//       object_log('return', $terms);
       return reset($terms)->id();
     } else {
       object_log('else', $value);
     }
   } else {
     object_log('else2', $value);
   }
//    object_log('return', $return);
    return ;
  }

}
