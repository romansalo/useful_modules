<?php

namespace Drupal\pfs_migrate\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\migrate_tools\MigrateBatchExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\migrate\MigrateMessage;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\migrate_tools\Form\MigrationExecuteForm;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\pfs_migrate\Service\ResourceMixinMigrateService;

/**
 * Provides base form for importers' settings.
 */
abstract class MigrateSettingsForm extends MigrationExecuteForm {

  use StringTranslationTrait;

  const MIGRATION_FILE_DIRECTORY = 'public://importer/';

  /**
   * The ResourceMixinMigrateService.
   *
   * @var $resourceService \Drupal\pfs_migrate\Service\ResourceMixinMigrateService
   *
   */
  protected $resourceService;

  /**
   * The state storage.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $stateStorage;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The tempStore.
   *
   * @var $tempStore \Drupal\Core\TempStore\PrivateTempStoreFactory
   *
   */
  protected $tempStore;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.migration'),
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('resources.migrate'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Constructs a new ImporterAccreditorsSettingsForm object.
   *
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration_plugin_manager
   *   The plugin manager for config entity-based migrations.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state storage.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\pfs_migrate\Service\ResourceMixinMigrateService $resource_service
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   */
  public function __construct(
    MigrationPluginManagerInterface $migration_plugin_manager,
    StateInterface $state,
    EntityTypeManagerInterface $entityTypeManager,
    ResourceMixinMigrateService $resource_service,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    parent::__construct($migration_plugin_manager);
    $this->stateStorage = $state;
    $this->entityTypeManager = $entityTypeManager;
    $this->resourceService = $resource_service;
    $this->tempStore = $temp_store_factory->get('pfs_migrate');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Show importer status.
    /** @var \Drupal\migrate\Plugin\Migration $migration */
    $migration = $this->migrationPluginManager->createInstance($this->getMigrationName());
    $form['status'] = [
      '#type' => 'item',
      '#title' => $this->t('Importer Status'),
      '#markup' => $migration->getStatusLabel(),
      '#weight' => -100,
    ];
    $migrate_last_imported_store = \Drupal::keyValue('migrate_last_imported');
    $last_imported = $migrate_last_imported_store->get($migration->id(), FALSE);
    $date_formatter = \Drupal::service('date.formatter');

    $form['url_list'] = [
      '#type' => 'textarea',
      '#cols' => 20,
      '#rows' => 15,
      '#title' => $this->t('Url links'),
      '#weight' => -100,
    ];
    $form['last_run'] = [
      '#type' => 'item',
      '#title' => $this->t('Last run'),
      '#markup' => $last_imported ? $date_formatter->format($last_imported / 1000, 'custom', 'Y-m-d H:i:s') : '',
      '#weight' => -100,
    ];
    $form['operations']['options']['update_geo_data'] = [
      '#type' => 'checkbox',
      '#title' => t('Update geo-data'),
      '#description' => t('Check this box to update all previously-imported content geo-data
      in addition to importing new data. Leave unchecked to only import
      new geo-data.'),
    ];
    $form['operations']['options']['log_geo_data'] = [
      '#type' => 'checkbox',
      '#title' => t('Log geo-data'),
      '#description' => t('Check this box to log all details about geo requests. It can create a big log files or db records and possible slow site.'),
      '#states' => [
        'visible' => [
          'input[name="update_geo_data"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => -99,
    ];

    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => ['::saveData'],
    ];

    $migrate_urls = $this->tempStore->get($this->getMigrationName() . '_migrate_urls');

    if ($migrate_urls) {
      $form['url_list']['#default_value'] = implode("\n", $migrate_urls);
    }

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function saveData(array &$form, FormStateInterface $form_state) {
    $migration_name = $this->getMigrationName();
    $data_array = preg_split('/\r\n|\r|\n/', $form_state->getValue('url_list'));
    $this->tempStore->set($migration_name . '_migrate_urls', $data_array);
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\migrate\MigrateException
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->saveData($form, $form_state);
    $operation = $form_state->getValue('operation');

    $limit = 0;
    if ($form_state->getValue('limit')) {
      $limit = $form_state->getValue('limit');
    }

    $update = 0;
    if ($form_state->getValue('update')) {
      $update = $form_state->getValue('update');
    }

    $force = 0;
    if ($form_state->getValue('force')) {
      $force = $form_state->getValue('force');
    }

    $update_geo_data = 0;
    if ($form_state->getValue('update_geo_data')) {
      $update_geo_data = $form_state->getValue('update_geo_data');
    }

    $log_geo_data = 0;
    if ($form_state->getValue('log_geo_data')) {
      $log_geo_data = $form_state->getValue('log_geo_data');
    }

    if ($form_state->getValue('url_list')) {
      $data_array = preg_split('/\r\n|\r|\n/', $form_state->getValue('url_list'));
    }
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
    $migration = $this->migrationPluginManager->createInstance($this->getMigrationName(), [
      'source' => [
        'path' => $this->resourceService->dataList($data_array),
      ],
    ]);
    $migrateMessage = new MigrateMessage();

    switch ($operation) {
      case 'import':
        $options = [
          'limit' => $limit,
          'update' => $update,
          'force' => $force,
          'update_geo_data' => $update_geo_data,
          'log_geo_data' => $log_geo_data,
        ];
        $executable = new MigrateBatchExecutable($migration, $migrateMessage, $options);
        $executable->batchImport();
        break;

      case 'rollback':
        $options = [
          'limit' => $limit,
          'update' => $update,
          'force' => $force,
          'update_geo_data' => $update_geo_data,
          'log_geo_data' => $log_geo_data,
        ];

        $executable = new MigrateBatchExecutable($migration, $migrateMessage, $options);
        $executable->rollback();
        break;

      case 'stop':
        $migration->interruptMigration(MigrationInterface::RESULT_STOPPED);
        break;

      case 'reset':
        $migration->setStatus(MigrationInterface::STATUS_IDLE);
        break;
    }
  }

  /**
   * Returns a unique string identifying the migration.
   *
   * @return string
   *   The unique string identifying the migration.
   */
  abstract public function getMigrationName();

}
