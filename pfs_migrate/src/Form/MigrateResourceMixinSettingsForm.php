<?php

namespace Drupal\pfs_migrate\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides form for organizations importer settings.
 */
class MigrateResourceMixinSettingsForm extends MigrateSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'resource_mixin__settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getMigrationName() {
    return 'resource_mixin__node';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['operations']['options']['update_geo_data']['#access'] = FALSE;
    $form['operations']['options']['log_geo_data']['#access'] = FALSE;
    return $form;
  }

}
